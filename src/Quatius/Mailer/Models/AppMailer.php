<?php

namespace Quatius\Mailer\Models;

use Mail;

class AppMailer
{
	public $fromName = "Administrator";
	
	public $fromNoReply = "noreply@mail.com";
	public $from = null;
	public $emailAdmin = "sale@mail.com";
	
	public $logType;
	
	public $to;
	
	public $cc = [];
	public $bcc = [];
	public $subject;
	
	public $view = "";
	
	public $data = [];
	
	public $attachPaths=[];
	
	public $sent = 0;
	public $body = "";

	public function __construct(){
		$this->fromName = config('quatius.mailer.from')['name'];
		$this->fromNoReply = config('quatius.mailer.from')['address'];
		$this->emailAdmin = config('quatius.mailer.admin');
		$this->logType = config('quatius.mailer.logType', 'general');
		
		$bcc = config('quatius.mailer.bcc', '');
		
		if ($bcc){
			if (is_array($bcc))
				$this->bcc = $bcc;
			else
				$this->bcc = [$bcc];
		}

		$cc = config('quatius.mailer.cc', '');
		if ($cc){
			if (is_array($cc))
				$this->cc = $cc;
			else
				$this->cc = [$cc];
		}
	}
	
	public static function sendEmailToAdmin($subject = "", $view, $data=[])
	{
	    return self::sendEmail("", $subject,$view, $data);
	}
	
	public static function sendEmail($to="", $subject = "", $view, $data=[], $from=null, $attachmentList=[],$type='general',$sendIt=true)
	{
		$appMailer = new AppMailer();
			
		if ($view != null)
		{
		    $appMailer->logType = $type;
		    $appMailer->to($to);
		    $appMailer->from($from);
			$appMailer->subject = $subject;
			$appMailer->view = $view;
			$appMailer->data = $data;
			$appMailer->attachments($attachmentList);
			if ($sendIt){
			    $appMailer->deliver();
			}
		}
		return $appMailer;
	}
	
	public function log($logType=null, $data=[]){
	    
	    $logType = $logType?:$this->logType;
	    
	    if (!$logType || !config('quatius.mailer.log-enable', false)) return;
	    $body = "";
	    if (config('quatius.mailer.save-body', false)){
	        $body = $this->view?view($this->view, $this->data)->render():$this->body;
	    }
	    
	    EmailLog::create([
	        'type'=>$logType,
	        "mail_to"=>$this->to,
	        "subject"=>$this->subject,
	        "body"=>$body,
	        "from_name"=>$this->fromName,
	        "mail_from"=>$this->from,
	        "view"=>$this->view,
	        "form_data"=>json_encode($data?:$this->data),
	        "attachs"=>json_encode($this->attachPaths)
	    ]);
	    return $this;
	}
	
	public function view($view, $data=[]){
	    $this->view =$view;
	    $this->data =$data;
	    return $this;
	}
	public function body($body){
	    return $this->text($body);
	}
	public function text($body, $data=[]){
	    $this->view = "";
	    $this->body = $body;
	    $this->data = $data;
	    return $this;
	}
	public function to($to=null){
	    $this->to =  $to?:$this->emailAdmin;
	    return $this;
	}
	public function cc($cc=[]){
	    $this->cc = $cc;
	    return $this;
	}
	public function bcc($bcc=[]){
	    $this->bcc = $bcc;
	    return $this;
	}
	public function from($from=null, $fromName=null){
	    $this->from = $from?:$this->fromNoReply;
	    $this->fromName = $fromName?:$this->fromName;
	    return $this;
	}
	
	public function attachments($pathList=[]){
		
	    $this->attachPaths= is_string($pathList)?[basename($pathList)=>$pathList]:$pathList;
	    return $this;
	}
	
	public function send($type="html")
	{
	    return $this->deliver($type);
	}
	
	public function deliver($type="html")
	{
	    
		if (!$this->to){
		    $this->to();
		}
		if (!$this->from){
		    if ($this->to == $this->emailAdmin){ // making sure that sender and reciever is not the same
		        $this->from($this->fromNoReply);
		    }
		    else{
		        $this->from($this->emailAdmin);
		    }
		}
		
		$data = $this->view?$this->data:["body_plain"=>$this->body];
		$view = $this->view?:"Mailer::emails.blank";

		if ($type=="pain"){
			$this->sent = Mail::plain($view, $data, function($message){
		        $message->mailer = $this;
		        $message->from($this->from, $this->fromName)
		        ->subject($this->subject)
		        ->to($this->to)
		        ->cc($this->cc)
		        ->bcc($this->bcc);
		        foreach ($this->attachPaths as $name=>$attachment){
		            if ($attachment != "" && file_exists($attachment)){
		                if (!is_numeric($name))
		                    $message->attach($attachment,['as' => $name]);
		                    else {
		                        $message->attach($attachment);
		                    }
		            }
		        }
			});
		}
		else if ($type=="raw"){
			$this->sent = Mail::raw($this->body, function($message){
		        $message->mailer = $this;
		        $message->from($this->from, $this->fromName)
		        ->subject($this->subject)
		        ->to($this->to)
		        ->cc($this->cc)
		        ->bcc($this->bcc);
		        foreach ($this->attachPaths as $name=>$attachment){
		            if ($attachment != "" && file_exists($attachment)){
		                if (!is_numeric($name))
		                    $message->attach($attachment,['as' => $name]);
		                    else {
		                        $message->attach($attachment);
		                    }
		            }
		        }
			});
		}else{
			$this->sent = Mail::send($view, $data, function($message){
				$message->mailer = $this;
				$message->from($this->from, $this->fromName)
					->subject($this->subject)
					->to($this->to)
					->cc($this->cc)
					->bcc($this->bcc);
					
				foreach ($this->attachPaths as $name=>$attachment){
					if ($attachment != "" && file_exists($attachment)){
						if (!is_numeric($name))
							$message->attach($attachment,['as' => $name]);
						else {
							$message->attach($attachment);
						}
					}
				}
			});
		}
		return $this;
	}
}
