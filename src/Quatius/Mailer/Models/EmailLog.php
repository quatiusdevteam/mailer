<?php

namespace Quatius\Mailer\Models;

use Illuminate\Database\Eloquent\Model;

class EmailLog extends Model {
    
    public $timestamps = false; // disable all behaviour
    
    protected $fillable = ['type', 'subject', 'body' , 'mail_to', 'from_name', 'mail_from', 'view', 'form_data', 'attachs'];

}
