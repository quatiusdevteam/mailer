<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type', 20)->nullable();
            $table->string('subject', 150)->nullable();
            $table->mediumText('body')->nullable();
            $table->string('mail_to', 200)->nullable();
            $table->string('from_name', 200)->nullable();
            $table->string('mail_from', 200)->nullable();
            $table->string('view', 200)->nullable();
            $table->text('form_data')->nullable();
            $table->text('attachs')->nullable();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_contacts');
    }
}
