<?php
if (!function_exists('mailer'))
{
    function mailer($subject="")
    {
        $mailer = new \Quatius\Mailer\Models\AppMailer();
        $mailer->subject = $subject;
        return $mailer;
    }
}

if (!function_exists('sendMailView'))
{
    function sendMailView($subject="", $view="", $data=[], $to="", $from=null, $attachment=[], $type="general", $sendIt=true)
	{
	    $fromName = "";
	    $fromEmail = $from;
	    if (is_array($from)){
	        $fromEmail = reset($from); // First Element's Value
	        $fromName = key($from); // First Element's Key
	        $fromName = is_numeric($fromName)?"":$fromName;
	    }
	    
	    $mailer = mailer($subject)
	               ->view($view,$data)
	               ->to($to)
	               ->from($fromEmail,$fromName)
	               ->attachments($attachment);
	    
       if ($sendIt){
           $mailer->send();
       }
       
       return $mailer;
           
	   // return \Quatius\Mailer\Models\AppMailer::sendEmail($to,$subject,$view, $data, $from, is_array($attachment)?$attachment:[$attachment], $type, $sendIt);
	}
}

if (!function_exists('sendPlainMail'))
{
	function sendPlainMail($subject="", $body="",$to="", $from=null, $attachment=[], $sendIt=true)
	{
	    $fromName = "";
	    $fromEmail = $from;
	    if (is_array($from)){
	        $fromEmail = reset($from); // First Element's Value
	        $fromName = key($from); // First Element's Key
	        $fromName = is_numeric($fromName)?"":$fromName;
	    }
	    
	    $mailer = mailer($subject)
            ->text($body)
    	    ->to($to)
			->from($fromEmail,$fromName)
			->attachments($attachment);
    	    
	    if ($sendIt){
	        $mailer->send();
	    }
	    
	    return $mailer;
	    
	    //return \Quatius\Mailer\Models\AppMailer::sendEmail($to,$subject,'Mailer::emails.blank', array_merge(['body_plain' => $body],$data), $from, [], $type, $sendIt);
	}
}
