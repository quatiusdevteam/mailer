<?php

return [
    'log-enable'=>false,
    'save-body'=>false,
    'admin' => env('APP_EMAIL_ADMIN_ADDRESS', 'admin@mail.com'),
    'bcc' => env('APP_EMAIL_BCC',[]),
    'cc' => env('APP_EMAIL_CC',[]),
    'from' => ['address' => env('APP_EMAIL_ADDRESS', 'noreply@mail.com'), 'name' => env('APP_EMAIL_NAME', 'online-web')],
];
