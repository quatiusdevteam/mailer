<?php

namespace Quatius\Mailer\Providers;

use Illuminate\Support\ServiceProvider;
use Event;
use Quatius\Mailer\Models\AppMailer;
/**
 * @author seyla.keth
 *
 */
class MailerProvider extends ServiceProvider
{
	
	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
	    if (config('quatius.mailer.log-enable', false)){
	        $this->loadMigrationsFrom(__DIR__.'/../Databases/migrations');
	    }
	}
	
	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
	    Event::listen('email.send-blank', function($subject="", $body="",$to="", $from=""){
	        AppMailer::sendEmail($to,$subject,'Mailer::emails.blank', $data=['body' => $body], $from);
	    });
	    
	    Event::listen('email.send-view', function($subject="", $view="", $data=[], $to="", $from="", $attachment=""){
            AppMailer::sendEmail($to,$subject,$view, $data, $from, is_array($attachment)?$attachment:[$attachment]);
        });
	}
	
	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return ['quatius'];
	}
}